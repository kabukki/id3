/*
 *
 *
 *
 */

#include "id3.h"

bool is_id3(void *start) {
  return (strncmp((char *)start, "ID3", 3) == 0);
}

t_id3 get_id3_header(void *start) {
  t_id3 data;
  
  data = *((t_id3 *)start);
  data.version[0] += '0';
  data.version[1] += '0';
  data.flags = 0b00101100;
  return data;
}

void print_binary(char x) {
  size_t i = 8;

  while (i--) {
    putchar('0' + ((x >> i) & 1));
  }
  putchar('\n');
}

void print_flags(char flags) {
  const static t_pair vf[] = {
    {"UNSYNC", FLAG_UNSYNC},
    {"EXTENDED", FLAG_EXTEND},
    {"EXPERIMENTAL", FLAG_EXP},
    {"FOOTER", FLAG_FOOTER},
    {NULL, FLAG_NONE}
  };
  char *comma = "";
  
  for (size_t i = 0; vf[i].name != NULL; i++) {
    if (flags & vf[i].value) {
      printf("%s%s", comma, vf[i].name);
      comma = ", ";
    }
  }
  printf("\n");
}

void id3(void *start) {
  t_id3 data;
  
  if (is_id3(start)) {
    data = get_id3_header(start);
    printf("ID3 version: 2.%c.%c\n",
	   data.version[MAJOR_VERSION], data.version[MINOR_VERSION]);
    printf("Flags: ");
    print_binary(data.flags);
    print_flags(data.flags);
    printf("Data offset: %d\n",
	   data.size);
  } else
    printf("No ID3v2 metadata in this file\n");
}

void id3_wrapper(const char *file) {
  struct stat s;
  void *start;
  int fd = open(file, O_RDONLY);

  if (fd != -1) {
    fstat(fd, &s);
    start = mmap(NULL, s.st_size, PROT_READ, MAP_PRIVATE, fd, 0);
    if (start != (void *) -1) {
      printf("\n%s:\n", file);
      id3(start);
      munmap(start, s.st_size);
    } else
      perror("mmap");
    close(fd);
  } else
    perror(file);
}

int main(int ac, char **av) {
  for (size_t i = 1; i < ac; i++) {
    id3_wrapper(av[i]);
  }
  return 0;
}
