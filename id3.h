#ifndef ID3_H_
#define ID3_H_

#include <sys/stat.h>
#include <sys/mman.h>
#include <stdio.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdbool.h>
#include <string.h>

typedef struct s_pair {
  char *name;
  int value;
} t_pair;

/* http://id3.org/id3v2.4.0-structure */

typedef struct s_id3 {
  char		ident[3]; /* file identification */
  char		version[2]; /* version numbers */
  char		flags; /* flags */
  int		size; /* id3 data size in bytes */
} t_id3;

#define MAJOR_VERSION 0
#define MINOR_VERSION 1

#define FLAG_NONE	0b00000000
#define FLAG_UNSYNC	0b00000010
#define FLAG_EXTEND	0b00000100
#define FLAG_EXP	0b00001000
#define FLAG_FOOTER	0b00010000

#endif /* ID3_H_ */
